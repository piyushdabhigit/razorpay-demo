// public/script.js
document.getElementById('pay').addEventListener('click', function () {
    const amount = document.getElementById('amount').value;
  
    fetch('/order', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ amount: amount }),
    })
      .then((response) => response.json())
      .then((data) => {
        const options = {
          key: 'YOUR_RAZORPAY_KEY_ID',
          amount: data.amount,
          currency: data.currency,
          name: 'Your Company Name',
          description: 'Test Payment',
          order_id: data.id,
          handler: function (response) {
            alert('Payment successful. Payment ID: ' + response.razorpay_payment_id);
          },
          prefill: {
            name: 'Test User',
            email: 'testuser@example.com',
            contact: '1234567890',
          },
          theme: {
            color: '#F37254',
          },
        };
        const rzp = new Razorpay(options);
        rzp.open();
      })
      .catch((error) => console.log(error));
  });
  