// server.js
const express = require('express');
const Razorpay = require('razorpay');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(express.static('public'));

const razorpay = new Razorpay({
  key_id: 'YOUR_RAZORPAY_KEY_ID',
  key_secret: 'YOUR_RAZORPAY_KEY_SECRET',
});

app.post('/order', (req, res) => {
  const { amount } = req.body;

  const options = {
    amount: amount,  // Amount in paise
    currency: 'INR',
  };

  razorpay.orders.create(options, (err, order) => {
    if (err) {
    console.log(err)
      res.status(500).json(err);
      return;
    }
    res.json(order);
  });
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
